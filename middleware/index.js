var Comment = require("../models/comment");
var Trip = require("../models/trip");

module.exports = {
    isLoggedIn: function(req, res, next){
        if(req.isAuthenticated()){
            return next();
        }
        res.redirect("/login");
    },
    checkUserCampground: function(req, res, next){
        if(req.isAuthenticated()){
            Trip.findById(req.params.id, function(err, trip){
               if(trip.author.id.equals(req.user._id)){
                   next();
               } else {
                   console.log("BADD!!!");
                   res.redirect("/trips/" + req.params.id);
               }
            });
        } else {
            res.redirect("/login");
        }
    },
    checkUserComment: function(req, res, next){
        console.log("YOU MADE IT!");
        if(req.isAuthenticated()){
            Comment.findById(req.params.commentId, function(err, comment){
               if(comment.author.id.equals(req.user._id)){
                   next();
               } else {
                   res.redirect("/trips/" + req.params.id);
               }
            });
        } else {
            res.redirect("login");
        }
    }
}