var mongoose = require("mongoose");
// SCHEMA SETUP
var tripsSchema = new mongoose.Schema({
    name: String,
    departure: String,
    destination: String,
    tripDate: String,
    price: Number,
    description: String,
    author: {
      id: {
         type: mongoose.Schema.Types.ObjectId,
         ref: "User"
      },
      username: String
   },
    comments: [
      {
         type: mongoose.Schema.Types.ObjectId,
         ref: "Comment"
      }
  ]
});

var Trip = mongoose.model("Trip", tripsSchema);

module.exports = mongoose.model("Trip", tripsSchema);