var mongoose = require("mongoose");
var Trip = require("./models/trip");
var Comment   = require("./models/comment");

var data = [
    {
        name: "Trip 1",
        departure: "Cloud's Rest",
        destination: "Seoul",
        tripDate: "31.08.1976",
        price: 50,
        description: "consectetur adipiscing elit. Sed augue velit, rhoncus in pulvinar ac, consectetur et odio. Nam vel "
    },
    {
        name: "Trip 2",
        departure: "Desert Mesa", 
        destination: "Seoul",
        tripDate: "21.08.1976",
        price: 20,
        description: "ante non nibh ullamcorper, in blandit metus aliquet. Pellentesque et feugiat augue. Sed non molestie magna. "
    },
    {
        name: "Trip 3",
        departure: "Canyon Floor", 
        destination: "Seoul",
        tripDate: "11.08.1976",
        price: 30,
        description: "Lorem ipsum dolor sit amet, arcu ultricies, euismod nisi mattis, tincidunt diam. Nullam volutpat "
    }
]

function seedDB(){
   //Remove all Trip
   Comment.remove({}, function(err){
        if(err){
            console.log(err);
        }
        console.log("removed Comment!");
        });
    Trip.remove({}, function(err){
        if(err){
            console.log(err);
        }
        console.log("removed Trip!");
    });
    //    add a few Trips
//   Trip.remove({}, function(err){
//         if(err){
//             console.log(err);
//         }
//         console.log("removed Trip!");
//          //add a few Trips
//         data.forEach(function(seed){
//             Trip.create(seed, function(err, trip){
//                 if(err){
//                     console.log(err)
//                 } else {
//                     console.log("added a Trip");
//                     //create a comment
//                     Comment.create(
//                         {
//                             text: "This place is great, but I wish there was internet",
//                             author: "Homer"
//                         }, function(err, comment){
//                             if(err){
//                                 console.log(err);
//                             } else {
//                                 trip.comments.push(comment);
//                                 trip.save();
//                                 console.log("Created new comment");
//                             }
//                         });
//                 }
//             });
//         });
//     }); 
    //add a few comments
}

module.exports = seedDB;