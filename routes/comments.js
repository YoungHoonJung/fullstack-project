var express = require("express");
var router  = express.Router({mergeParams: true});
var Trip = require("../models/trip");
var Comment = require("../models/comment");
var middleware = require("../middleware");

// ====================
// COMMENTS ROUTES
// ====================

router.get("/new", middleware.isLoggedIn, function(req, res){
    // find campground by id
    Trip.findById(req.params.id, function(err, trip){
        if(err){
            console.log(err);
        } else {
             res.render("comments/new", {trip: trip});
        }
    })
});

router.post("/",middleware.isLoggedIn,function(req, res){
   //lookup campground using ID
   Trip.findById(req.params.id, function(err, trip){
       if(err){
           console.log(err);
           res.redirect("/trips");
       } else {
        Comment.create(req.body.comment, function(err, comment){
           if(err){
               console.log(err);
           } else {
               //add username and id to comment
               comment.author.id = req.user._id;
               comment.author.username = req.user.username;
               //save comment
               comment.save();
               trip.comments.push(comment);
               trip.save();
               console.log(comment);
               res.redirect('/trips/' + trip._id);
           }
        });
       }
   });
   //create new comment
   //connect new comment to campground
   //redirect campground show page
});


// COMMENT EDIT ROUTE
router.get("/:commentId/edit", middleware.isLoggedIn, function(req, res){
    // find campground by id
    Comment.findById(req.params.commentId, function(err, comment){
        if(err){
            console.log(err);
        } else {
             res.render("comments/edit", {trip_id: req.params.id, comment: comment});
        }
    })
});

// COMMENT UPDATE
router.put("/:commentId", function(req, res){
   Comment.findByIdAndUpdate(req.params.commentId, req.body.comment, function(err, comment){
       if(err){
           res.render("edit");
       } else {
           res.redirect("/trips/" + req.params.id);
       }
   }); 
});

// COMMENT DESTROY ROUTE
router.delete("/:commentId",middleware.checkUserComment, function(req, res){
    Comment.findByIdAndRemove(req.params.commentId, function(err){
        if(err){
            console.log("PROBLEM!");
        } else {
            res.redirect("/trips/" + req.params.id);
        }
    })
});

module.exports = router;