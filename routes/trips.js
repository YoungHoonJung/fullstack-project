var express = require("express");
var router  = express.Router();
var Trip = require("../models/trip");
var middleware = require("../middleware");

// ====================
// Trips ROUTES
// ====================
//INDEX - show all trips
router.get("/", function(req, res){
        // Get all campgrounds from DB
    Trip.find({}, function(err, allTrips){
       if(err){
           console.log(err);
       } else {
          res.render("trips/index",{trips:allTrips});
       }
    });
});

router.post("/", middleware.isLoggedIn, function(req, res){
    var name = req.body.name;
    var departure = req.body.departure;
    var destination = req.body.destination;
    var tripDate = req.body.tripDate;
    var price = req.body.price;
    var description = req.body.description;
    var author = {
        id: req.user._id,
        username: req.user.username
    }
    var newTrip = 
    {
        name: name,
        departure: departure,
        destination: destination,
        description:description,
        tripDate:tripDate,
        price: price,
        author:author
    };

    // Create a new campground and save to DB
    Trip.create(newTrip, function(err, newlyCreated){
        if(err){
            console.log(err);
        } else {
            //redirect back to trip page
            res.redirect("/trips");
        }
    });
});

router.get("/new",middleware.isLoggedIn,function(req, res) {
   res.render("trips/new"); 
});


// SHOW - shows more info about one trip
router.get("/:id", function(req, res){
    //find the trip with provided ID
    Trip.findById(req.params.id).populate("comments").exec(function(err, foundTrip){
        if(err){
            console.log(err);
        } else {
            //render show template with that trip
            res.render("trips/show", {trip: foundTrip});
        }
    });
})

// EDIT CAMPGROUND ROUTE
//middleware.checkTripOwnership,
router.get("/:id/edit",middleware.checkUserCampground, function(req, res){
    console.log("IN EDIT!");
    //find the campground with provided ID
    Trip.findById(req.params.id, function(err, foundTrip){
        if(err){
            console.log(err);
        } else {
            //render show template with that campground
            res.render("trips/edit", {trip: foundTrip});
        }
    });
});

// UPDATE CAMPGROUND ROUTE
//middleware.checkTripOwnership,
router.put("/:id", function(req, res){
    // find and update the correct campground
    Trip.findByIdAndUpdate(req.params.id, req.body.trip, function(err, updatedTrip){
       if(err){
           console.log( err.message);
           res.redirect("/trips");
       } else {
           //redirect somewhere(show page)
           console.log("Successfully Updated!");
           res.redirect("/trips/" + req.params.id);
       }
    });
});

// DESTROY CAMPGROUND ROUTE
router.delete("/:id", function(req, res){
   Trip.findByIdAndRemove(req.params.id, function(err){
      if(err){
          res.redirect("/trips");
      } else {
          res.redirect("/trips");
      }
   });
});

module.exports = router;